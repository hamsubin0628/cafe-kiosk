import React, {useCallback, useState} from 'react';
import MenuCard from "./MenuCard";
import CartInItem from "./CartInItem";

const CafeMenu = () => {
    const [cart, setCart] = useState([]);

    const menuItems = [
        { menuName: "아이스 아메리카노", price: 4500, imgUrl: "/images/ice-americano2.png" },
        { menuName: "아이스 카페라떼", price: 5500, imgUrl: "/images/ice-cafe-ratte.png" },
        { menuName: "아이스 헤이즐넛", price: 5000, imgUrl: "/images/ice-americano.png" },
        { menuName: "자몽 허니 블랙티", price: 4500, imgUrl: "/images/jamong.png" },
        { menuName: "바닐라 플랫 화이트", price: 5900, imgUrl: "/images/banila.png" },
        { menuName: "콜드브루 오트 라떼", price: 5800, imgUrl: "/images/oat.png" },
        { menuName: "유자 민트 티", price: 5900, imgUrl: "/images/mint.png" },
        { menuName: "블론드 스타벅스 돌체 라떼", price: 5900, imgUrl: "/images/dalche.png" }
    ]

    // 콜백 함수를 자식 컴포넌트에게 넘겨서 사용 => 메모이제이션
    const addToCart = useCallback(menu => {
        // { menuName: "아이스 아메리카노", price: 4500, imgUrl: "/images/ice-americano2.png", count : 1 },
        // {...menu, count: 1}
        setCart(prevCart => {
            // existingItem = item을 돌면서 메뉴 이름이 같은것을 찾아와
            const existingItem = prevCart.find((item) => item.menuName === menu.menuName)

            // existingItem이 존재하면 prevCart의 메뉴이름과 메뉴의 메뉴이름이 같으면 카운트 +1 증가, 같지 않으면 item
            if (existingItem) {
                return prevCart.map((item) =>
                    item.menuName === menu.menuName
                    ? { ...item, count: item.count + 1 } : item
                )

            // existingItem이 존재하지 않으면 count 1
            } else {
                return [...prevCart, { ...menu, count: 1 }]
            }
        })
    }, [])
    // [] => 전부 다

    const removeFromCart = useCallback(menu => {
        setCart(prevCart => {
            return prevCart.reduce((acc, item) => {
                // 돌면서 메뉴 이름이 같으면서
                if (item.menuName === menu.menuName) {
                    // 카운트가 1보다 크면
                    if(item.count > 1) {
                        return [...acc, {...item, count: item.count - 1 }]
                    }
                // 조건이 만족하지 않으면 카운트 아예 삭제
                } else {
                    return [...acc, item]
                }
                return acc
            }, [])
        })
    }, [])

    // 총 금액
    const calculateTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.count, 0);
    }, [cart])

    return (
        <div>
            <div className="hsb-cafe-menu">
                {menuItems.map((menu) => (
                    <MenuCard key={menu.menuName} menu={menu} addToCart={addToCart}/>
                ))}
                <div className="hsb-pay-section">
                    {cart.map(item => (
                        <CartInItem key={item.menuName} item={item} removeFromCart={removeFromCart}/>
                    ))}
                </div>
            </div>
            <p className="hsb-total-price">총 금액 : {calculateTotal()}원 </p>
        </div>
    )
}

export default CafeMenu;
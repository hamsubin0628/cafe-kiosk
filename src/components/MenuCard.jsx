import React from 'react';

const MenuCard = ({menu, addToCart}) => (
    <div className="hsb-menu-select">
        <div className="hsb-coffee" role="button" onClick={() => {addToCart(menu)}}>
            <div className="hsb-menu-img">
                <img className="hsb-coffee-img" src={menu.imgUrl}/>
            </div>
            <p className="hsb-menu-name">{menu.menuName}</p>
            <p className="hsb-menu-price">{menu.price}원</p>
        </div>
    </div>
)

export default MenuCard;
import React from 'react';

const CartInItem = ({ item, removeFromCart }) => {
    return (
        <div className="hsb-pay">
            <p className="hsb-menu-pay-name">{item.menuName}</p>
            <div className="hsb-remove">
                <p className="hsb-count">수량 : {item.count}개</p>
                <div className="hsb-remove-right">
                    <p className="hsb-menu-price">{item.count * item.price} 원</p>
                    <button className="hsb-remove-btn" onClick={() => removeFromCart(item)}>X</button>
                </div>
            </div>
            <div className="hsb-line"></div>
        </div>
    )
}

export default CartInItem;
import React from 'react';

const TestPage1 = () => {
    const menus = [
        { photoUrl : "1.jpg", name : "아아", price : 2500 },
        { photoUrl : "1.jpg", name : "아아2", price : 3000 },
        { photoUrl : "1.jpg", name : "아아3", price : 3500 },
        { photoUrl : "1.jpg", name : "아아4", price : 4000 },
    ]

    return (
        <div>
            {menus.map(menu => (
                <div>
                    <div>{menu.photoUrl}</div>
                    <div>{menu.name}</div>
                    <div>{menu.price}원</div>
                </div>
            ))}
        </div>
    )
}

export default TestPage1;
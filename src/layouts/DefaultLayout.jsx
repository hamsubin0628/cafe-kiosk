import React from 'react';

const DefaultLayout = ({ children }) => {
    return (
        <>
        <div className="hsb-header">
            <h1 className="hsb-logo">STARBUCKS</h1>
            <p className="hsb-sub-title">STARBUCKS COFFEE</p>
        </div>
            <main>{children}</main>
            <footer className="hsb-footer">
                <p className="hsb-footer-text">STARBUCKS</p>
            </footer>
        </>
    )
}

export default DefaultLayout;